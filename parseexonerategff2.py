#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# parse_exonerate_gff2.py
# This script parses the GFF (v2) table generated in the standard output of exonerate (v2.4.0) and uses bedtools (v2.30.0) to extract the transcripts.

##
# LIBRARIES
##

import argparse, re, sys, subprocess
from Bio import SeqIO

##
# FUNCTIONS
##

def printo(var):
	return sys.stdout.write(str(var).strip() + "\n")

def printe(var):
	return sys.stderr.write(str(var).strip() + "\n")

def read_ggf2_lines(args):
	lines = []
	with open(args.gff2, "r") as handle:
		for line in handle.readlines():
			line = line.strip()
			if line:
				line = [i.strip() for i in line.split("\t")]
				if len(line) == 9:
					reference, source, method, start_position, stop_position, score, strand, phase, group = line
					lines.append([reference, source, method, start_position, stop_position, score, strand, phase, group])
				elif len(line) == 8 and "cds" in line:
					reference, source, method, start_position, stop_position, score, strand, phase = line
					group = "."
					lines.append([reference, source, method, start_position, stop_position, score, strand, phase, group])
	return lines

def group_lines_per_gene(lines):
	gene_count = 0
	genes = {}
	references = {}
	for line in lines:
		reference, source, method, start_position, stop_position, score, strand, phase, group = line
		if method == "gene":
			gene_count += 1
			group = re.sub("gene\_id\s+\d+", "gene_id {}".format(gene_count), group)
			if not gene_count in genes:
				genes[gene_count] = []
		genes[gene_count].append([reference, source, method, start_position, stop_position, score, strand, phase, group])
		if not reference in references:
			references[reference] = []
		if not gene_count in references[reference]:
			references[reference].append(gene_count)
	return genes, references

def select_overlapping_genes(genes_list, genes):
	selection = []
	for gene_count in genes_list:
		include = True
		reference, source, method, st, sp, sc, strand, phase, group = genes[gene_count][0]
		st = int(st)
		sp = int(sp)
		sc = float(sc)
		for i in range(0, len(selection)):
			ST, SP, SC, GN = selection[i]
			if (sp < ST) or (SP < st):
				pass
			elif (st <= ST and ST <= sp) or (st <= SP and SP <= sp) or (ST <= st and st <= SP) or (ST <= sp and sp <= SP) or (st == SP and sp == SP): # Check for overlap
				include = False
				if sc >= SC: # Keep the alignment that has the highest score (or the last one found)
					selection[i] = [st, sp, sc, gene_count] # Replace in list
				else:
					pass
			else:
				printe("Error 00: Unexpected condition.")
				exit()
		if include == True:
			selection.append([st, sp, sc, gene_count]) # Add to list
	selection = list(set([i[3] for i in selection]))
	genes_to_delete = [i for i in genes_list if not i in selection]
	return genes_to_delete

def prepare_gff2_print(genes, name):
	new_gff2 = ""
	for gene_count in genes:
		gene_id = "{}{}".format(name, gene_count)
		transcript_id = "{}.t1".format(gene_id)
		for line in genes[gene_count]: # Edite column 9 (group)
			reference, source, method, start_position, stop_position, score, strand, phase, group = line
			if "gene_id" in group:
				group = re.sub("gene\_id\s+([^;]+)+" ,"gene_id \"{}\"".format(gene_id) , group)
			if "transcript_id" in group:
				group = re.sub("transcript\_id\s+([^;]+)+" ,"transcript_id \"{}\"".format(gene_id) , group)
			if (not "gene_id" in group) and (not "transcript_id" in group):
				group = "transcript_id \"{}\"; gene_id \"{}\"; {}".format(transcript_id, gene_id, group)
			new_gff2 += "{}\n".format("\t".join([reference, source, method, start_position, stop_position, score, strand, phase, group]))
	return new_gff2

def get_fasta_from_gff2(gff2_output, args):
	command = "bedtools getfasta -s -split -fi {} -bed {} -fo {}.fasta".format(args.fasta, gff2_output, args.output)
	process = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, universal_newlines=True)
	output = process.stdout
	return

def main(args):
	printe("# 1. Reading from GFF2 file...")
	lines = read_ggf2_lines(args)
	printe("# 2. Processing genes...")
	genes, references = group_lines_per_gene(lines)
	printe("# 3. Processing references...")
	all_references = [reference for reference in references]
	if args.select:
		printe("# 3.1. Selecting best match among overlapping genes...")
		for reference in all_references:
			genes_list = [gene_count for gene_count in references[reference]]
			genes_to_delete = select_overlapping_genes(genes_list, genes)
			for gene_count in genes_to_delete:
				del genes[gene_count]
	printe("# 4. Printing results...")
	new_gff2 = prepare_gff2_print(genes, args.name)
	gff2_output = "{}.gff2".format(args.output)
	handle = open(gff2_output, "w")
	handle.write(new_gff2)
	handle.close()
	if args.fasta:
		get_fasta_from_gff2(gff2_output, args)
	return

##
# INITIALIZATION
##

if __name__ == '__main__':
	# The arguments are defined in the next few lines
	parser=argparse.ArgumentParser()
	parser.add_argument("-f", "--fasta", help = "Input file with target sequences in FASTA format.", metavar = "FILE NAME", type = str, required = False)
	parser.add_argument("-g", "--gff2", help = "Input file with output GFF2 table from exonerate.", metavar = "FILE NAME", type = str, required = True)
	parser.add_argument("-n", "--name", help = "Prefix for gene code (default = g).", metavar = "PREFIX", type = str, required = False, default = "g")
	parser.add_argument("-o", "--output", help = "Prefix for the output file names (default = output).", metavar = "PREFIX", type = str, required = False, default = "output")
	parser.add_argument("-s", "--select", help="Select only the best hit among overlapping genes", action="store_true")
	args = parser.parse_args()
	main(args) # This will call the main fuction

exit() # Quit this script
