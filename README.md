# parseExonerateGff2

This script parses the [GFF (v2)](http://gmod.org/wiki/GFF2) table generated in the standard output of [`exonerate` (v2.4.0)](https://www.ebi.ac.uk/about/vertebrate-genomics/software/exonerate) and uses [`bedtools` (v2.30.0)](https://bedtools.readthedocs.io/en/latest/index.html) to extract the transcripts. We hope this script works for other (newer) versions of `exonerate`, but we make no promises.

## Dependencies

This script requires `Python` v3+ with the following modules (that should have come with it):

- `argparse`
- `re`
- `sys`
- `subprocess`

You will also need working installation of [`bedtools`](https://bedtools.readthedocs.io/en/latest/index.html) if you want to produce the target transcript sequences.

## Help

If you execute `python3 parseexonerategff2.py -h` or `python3 parseexonerategff2.py --help`, you will see the following usage information:

```
usage: parseexonerategff2.py [-h] -f FILE NAME -g FILE NAME [-n PREFIX] [-o PREFIX]

optional arguments:
  -h, --help            show this help message and exit
  -f FILE NAME, --fasta FILE NAME
                        Input file with target sequences in FASTA format.
  -g FILE NAME, --gff2 FILE NAME
                        Input file with output GFF2 table from exonerate.
  -n PREFIX, --name PREFIX
                        Prefix for gene code (default = g).
  -o PREFIX, --output PREFIX
                        Prefix for the output file names (default = output).
```

## Arguments

### -f/--fasta

**Not required**. Input file with target (annotated) sequences in FASTA format. If provided, the `parseExonerateGff2` script will attempt to extract transcripts with the help of `bedtools`.

### -g/ --gff2

**REQUIRED**. GFF2 is a supported format in GMOD, but it is now deprecated and if you have a choice you should use GFF3. The program `exonerate` v2.4.0 produces tables in GFF2 format when the option `--showtargetgff yes` is used. The argument option `-g` or `--ggf2` in `parseExonerateGff2` serves to indicate the GFF2 table produced by `exonerate`.

### -n/ --name

**Not required**. Prefix for the gene code (**not gene name**). The default option in `g`, which would in gene codes `g1`, `g2`, ..., `gn`.

### -o/ --output

**Not required**. Prefix for the name of the output file names. The default option is `output`.

## Example

This script comes with two example files: a target FASTA file named `example.fasta` and a GFF2 table named `example.gff`. You can test if everything is working by moving to the directory where all these files are located and executing the following:

```
python3 parseexonerategff2.py -f example.fasta -g example.gff
```

This will generate a new GGF2 table (`output.ggf2`) and FASTA (`output.fasta`) files. The GFF2 table will have only the best hits per position, and the gene and transcript codes will be renamed accordingly. The FASTA file will have the transcripts.
